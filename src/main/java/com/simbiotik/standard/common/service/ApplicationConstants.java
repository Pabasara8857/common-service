package com.simbiotik.standard.common.service;

public class ApplicationConstants {

  private ApplicationConstants() {
    super();
  }

  public static String GET = "GET: get agent by id";
  public static String DELETE = "DELETE: delete agent";
  public static String GET_ALL = "GET: get all products";
  public static final String MESSAGE = "message";
  public static final String PRODUCT = "product";
  public static final String PRODUCT_NOT_FOUND_FOR_ID = "product.not.found.for.id";

  public static final String SUCCESS = "SUCCESS";
  public static final String ERROR = "ERROR";
  public static final String UNSUCCESS = "UNSUCCESS";

  public static final String GLOBAL_DATE_FORMAT = "dd/MM/yyyy";
  public static final String GLOBAL_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm A";

  public static final String SUBMITTED_FOR_DELIVERY = "SUBMITTED_FOR_DELIVERY";

  public static final int STATUS_CODE_OK = 200;

  public static final String MAX_ALLOWED_CHARACTERS = "MAX_ALLOWED_CHARACTERS : ";
  public static final String MAX_ALLOWED_CHARACTERS_BETWEEN = "MAX_ALLOWED_CHARACTERS BETWEEN : ";
  public static final String VALUE_ALREADY_PRESENT = "VALUE ALREADY PRESENT : ";

  public static final String AGENT_NOT_FOUND_FOR_ID = "agent.not.found.for.id";

}
